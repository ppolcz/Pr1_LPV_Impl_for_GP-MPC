function [Pl,Sh] = Mf_Plot_MeanVar(tt,xx,xx_std,Color)
arguments
    tt,xx,xx_std,
    Color = [0 0.4470 0.7410]
end
%%
%  File: Mf_Plot_MeanVar.m
%  Directory: 5_Sztaki20_Main/Models/01_QArm/v13_2022_01_27_rontsuk_tovabb/9_lepes_Cikkhez_Minden
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2022. January 27. (2021b)
%


alpha = 2;
Pl = plot(tt,xx,'Color',Color);

Sh = shade(tt,xx'+alpha*xx_std',tt,xx-alpha*xx_std,'FillType',[1 2;2 1],'FillColor',Color);
Sh(1).LineStyle = 'none';
Sh(2).LineStyle = 'none';
Sh(3).FaceAlpha = 0.2;

end