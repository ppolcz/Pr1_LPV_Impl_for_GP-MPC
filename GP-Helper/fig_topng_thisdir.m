function [ret] = fig_topng_thisdir(fig,fname)
%%
%  File: fig_topng_thisdir.m
%  Directory: 5_Sztaki20_Main/Models/01_QArm/v4_2021_05_21/Helper
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. May 27. (2021a)
%

% fileparts
fp = pcz_mfilename(fname);

DIR_ES = [ getenv('local_EXTERNAL_SOURCE_DIR') filesep fp.dirs{1} ];

figname = [ DIR_ES filesep num2str(fig.Number) '_' strrep(fig.Name,' ','_') '.png' ];

DIR = fileparts(figname);
if ~exist(DIR,"dir")
    mkdir(DIR)
end

exportgraphics(fig,figname);

end