function [Pl_sample,Pl,Sh,GP_mean,GP_var] = GP_plot_1D(hyp,x)
%%
%  File: GP_plot_1D.m
%  Directory: 5_Sztaki20_Main/Tanulas/11_Exact_Moment_Matching
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. June 18. (2021a)
%

[GP_mean,GP_var] = GP_eval(x,hyp);

Color_Shade = [ 0.85 , 0.325 , 0.098 ];
Color_Sample = [ 0 , 0 , 0 ];

GP_std = sqrt(GP_var);

Pl_sample = plot(hyp.X,hyp.y,'.','Color',Color_Sample);
hold on

xlabel('$x$','Interpreter','latex','FontSize',14)
ylabel('$f(x)$','Interpreter','latex','FontSize',14)

Logger.latexify_axis(gca,12);

[Pl,Sh] = Mf_Plot_MeanVar(x,GP_mean,GP_std,Color_Shade);

grid on

end