function [out1,out2,out3,out4,Ts] = GP_Model_1(Type)
arguments
    Type (1,:) char {mustBeMember(Type,{'CT-nom','DT-GP'})} = 'DT-GP'
end
%%
%  File: GP_Model_1.m
%  Directory: 5_Sztaki20_Main/Models/02_Academic_2D_1GP/Variant_01
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. December 22. (2021b)
%

if nargin == 0 && nargout == 0
    plot_GP
    return
end

s.b = 5;
s.c = 0.5;

% GP selector matrix
Ed = [0;1];

% Output matrix
C = [1 0];

if strcmp(Type,'CT-nom')
    % Returns: [f,g,Ed,C]
    out1 = @Nominal_Model;
    out2 = @(x) Unknown_Model(x,s);
    out3 = Ed;
    out4 = C;
    return
end

%% Nominal model

Ts = 0.02;

out1 = @(x,u) x + Ts*Nominal_Model(x,u);

%% Unknown model

load('GP_Args.mat', 'hyp')

%% Outputs

% GP selector matrix
out2 = Ed*Ts;

% Output matrix
out3 = C;

% GP hyperparameters
out4 = hyp;

end

function f = Nominal_Model(x,u)

x1 = x(1,:);
x2 = x(2,:);

% Van der Pol
f = [
    x2
    1*(1-x1^2)*x2 - x1 + u
    ];

end

function g = Unknown_Model(x,args)

s.a = 0.3;
s.b = 5;
s.c = 0.797963;
s.d = 1;
s = parsepropval(s,args);

a = s.a;
b = s.b;
c = s.c;
d = s.d;

xTr = x-c;
xTrSc = xTr*a;

Stribeck = xTrSc .* (1 ./ ( xTrSc.^2 + 0.02) + 1 ) / a;
g = ( 10*sin(xTr/5) + 3*cos(xTr/3.2) + sin(xTr) + xTr + Stribeck ) / b + d;

end

function plot_GP
%%

[f,Ed,C,hyp,Ts] = GP_Model_1;
[~,g] = GP_Model_1("CT-nom");

XLim = [-15,15];

% Generate test points over a grid (to plot the Gaussian Process)
Nr_Grid_Points = 1501;
xx = linspace(XLim(1),XLim(2),Nr_Grid_Points)';

fig = figure(1);
fig.Position([3 4]) = [509 390];
delete(fig.Children);
ax = axes('Parent',fig);

[Pl_Samples,Pl,Sh] = GP_plot_1D(hyp,xx);
Pl_Ukn = plot(xx,g(xx),'k');
ax.XLim = XLim;

Leg = legend([Pl_Ukn Pl_Samples Pl Sh(3)],[ ...
    "Unknown function", ...
    "Measurements", ...
    "GP mean", ...
    "$2~\times$ std. deviation", ...
    % "$1 \times $\,St.dev. of $\hat \beta$ $(\approx 68\%)$", ...
    % "$2 \times $\,St.dev. of $\hat \beta$ $(\approx 95\%)$"
    ],'Location','northwest','Interpreter','latex','FontSize',12);

ax.XLabel.String = '$x_2$';
ax.YLabel.String = '$g(x_2)$';

exportgraphics(ax,'results/GP-model.pdf','ContentType','vector')
exportgraphics(ax,'results/GP-model.png')

end
