function yref = Reference_output_1(Ts,N,v)
%%
%  File: Reference_output_1.m
%  Directory: 5_Sztaki20_Main/Models/02_Academic_2D_1GP/Variant_01
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. December 22. (2021b)
%

%% 1. Negyzet

tk = [0 1 1.001 2 3 5 6];
yk = [1 1 0     0 0 1 1];

N1 = (numel(tk)-1)/Ts/v;
T1 = Ts*N1;
t1 = 0:Ts:T1;

tk = tk / tk(end) * T1;
y1 = interp1(tk,yk,t1);

%% 2. Sigmoid

Height = 2;

N2 = 4/Ts/v;
t = linspace(-5,7,N2);
y2 = -1 ./ (1 + exp(-t));

y2 = y2 - y2(end);
y2 = y2 / y2(1) * Height;
y2 = y2 - y2(1) + y1(end);

%% 3. Trigonometric

N3 = 8/Ts/v;

t = linspace(0,2.8*pi,N3);
y3 = 1-cos(t) + y2(end);

%% 4. Smooth random

N4 = 10/Ts/v;
t = linspace(0,2*pi,N4);

% 20,38,48
rng('shuffle')
Seed = round(rand*100);
rng(48)
y4 = cumsum(randn(1,N4)) + cumsum(rand(1,N4)-0.5) + sin(3.2*t) + cos(5.2*t) + sin(14.7*t);
y4 = conv(y4,hamming(round(N4/10)),'same');

y4 = y4-min(y4);
y4 = y4/max(y4);
y4 = y4-y4(1)+y3(end);

%% Final

yref = [y1 y2 y3 y4];

end

function plotit
%%

Ts = 0.1;
v = 2;
N = 100;
y = Reference_output_1(Ts,N,v);

plot(y,'.-')

end