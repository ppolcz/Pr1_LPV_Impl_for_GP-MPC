%%
%  File: msc_qarm_gpmpc_plotter.m
%  Directory: 5_Sztaki20_Main/Models/01_QArm/v10_2021_11_03_closed_loop/5_lepes_parhuzamos_szimulacio
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. November 03. (2021b)
%
%#ok<*GVMIS> 

startup

global G_COLOR_1 G_COLOR_2 G_COLOR_3 G_COLOR_4 G_COLOR_5 G_COLOR_6 G_COLOR_7
G_COLOR_1 = [0 0.4470 0.7410];
G_COLOR_2 = [0.8500 0.3250 0.0980];
G_COLOR_3 = [0.9290 0.6940 0.1250];
G_COLOR_4 = [0.4940 0.1840 0.5560];
G_COLOR_5 = [0.4660 0.6740 0.1880];
G_COLOR_6 = [0.3010 0.7450 0.9330];
G_COLOR_7 = [0.6350 0.0780 0.1840];

RunID = '22Feb05';
ScriptID = 'Acad_Mod1_EMM_NMPC';

Summary_fh = @(scriptID) sprintf('results/%s_%s_Summary.mat',scriptID,RunID);
load(Summary_fh(ScriptID));

N_Steady = 12;
N_exec = find(sum(Mx_traj.^2,1) > 0,1,'last');
t_exec = (N_Steady:N_exec-1) * Ts;
x_exec = Mx_traj(:,N_Steady+1:N_exec);
y_exec = x_exec(1,:);

y_ref = y_ref_all(:,N_Steady+1:N_exec);

lInf_Diff = max(vecnorm(y_exec - y_ref,2,1))
lInf_Ref = max(vecnorm(y_ref,2,1));
lInf_Traj = max(vecnorm(y_exec,2,1));

l2_Diff = norm(vecnorm(y_exec - y_ref,2,1),2);
l2_Ref = norm(vecnorm(y_ref,2,1),2);
l2_Traj = norm(vecnorm(y_exec,2,1),2);

%%

fig = plotter_SIM_Ref(Fnames{end},1231,14);
fig.Position(3:4) = [1009 203];
exportgraphics(fig,'/home/ppolcz/_/5_Sztaki20_Main/LaTeX/04_LTV-MPC_contouring/actual/fig/Academic/NMPC_Ref.pdf','ContentType','vector')

%% 
% Visualization with exact moment matching and simulated trajectories
for i = [1 100 201]
    fig = plotter_SIM(Fnames{i},1,5);
    fig.Position(3:4) = [1009 203];
    % keyboard

    [dn,bn,~] = fileparts(Fnames{i});
    export_name = [ dn filesep bn ];

    exportgraphics(fig,[export_name '.pdf'],'ContentType','vector')
    exportgraphics(fig,[export_name '.png'])
end

return

%%
% Visualize QArm with reference trajectory
load(Summary_fh('EMM','_tol03',3));
fps = 14;

N_exec = find(sum(mu_x_traj.^2,1) > 0,1,'last');
t_exec = (0:N_exec-1) * Ts;
x_exec = mu_x_traj(:,1:N_exec);

Trajectory = qarm_reference_1(Ts);
N_period = Trajectory.T/Ts;
r_exec = Trajectory.x(1:N_period,:)';

[t_,x_,p_,fig] = qarm_plot_ref(t_exec,x_exec,r_exec,fps,'plot',9081);
ax = fig.Children(1);
delete(fig.Children(2:3))

ax.XLim = [-0.1 0.5];
ax.YLim = [-0.4 0.4];
ax.ZLim = [0 0.8];
ax.View = [-5 20];

box off

Logger.latexify_axis(ax,12);
Logger.latexified_labels(ax,16,'$x$','$y$','$z$');
exportgraphics(ax,DIR_S('QArm_EMM_Hybrid_tol03.pdf'),'ContentType','vector')

%%
% Visualize QArm with reference trajectory
load(Summary_fh('Taylor','',3));
fps = 14;

N_exec = find(sum(mu_x_traj.^2,1) > 0,1,'last');
t_exec = (0:N_exec-1) * Ts;
x_exec = mu_x_traj(:,1:N_exec);

Trajectory = qarm_reference_1(Ts);
N_period = Trajectory.T/Ts;
r_exec = Trajectory.x(1:N_period,:)';

[t_,x_,p_,fig] = qarm_plot_ref(t_exec,x_exec,r_exec,fps,'plot',9081);
ax = fig.Children(1);
delete(fig.Children(2:3))

ax.XLim = [-0.1 0.5];
ax.YLim = [-0.4 0.4];
ax.ZLim = [0 0.8];
ax.View = [-5 20];

box off

Logger.latexify_axis(ax,12);
Logger.latexified_labels(ax,16,'$x$','$y$','$z$');
exportgraphics(ax,DIR_S('QArm_Taylor_Hybrid.pdf'),'ContentType','vector')

%%

s = qarm_params;
qarm_plot(s.x0)

%%

function fig = plotter_SIM(fname,fignr,XMax)
arguments
    fname,fignr
    XMax = Inf
end
    global G_COLOR_1 G_COLOR_2 G_COLOR_3 G_COLOR_4 G_COLOR_5 G_COLOR_6 G_COLOR_7 %#ok<NUSED> 

    Color_MPC = G_COLOR_1;
    Color_SIM = G_COLOR_2;

    load(fname, ...
        'N_Simulation','N_exec','Ts','nx','nu','ny','y_ref_all', ...
        'Mx_traj','Mu_traj','Mu','Mx','Sx','k','N');

    Title = sprintf([
        'State trajectories computed by MV-NMPC compared to the actual system trajectories. ' ...
        'Step $k = %d/%d$.'
        ],k,N_exec);

    fig = fig_new(100+fignr);
    tl = tiledlayout(1,3);
    tl.Padding = 'compact';
    tl.TileSpacing = 'tight';
    
    XLim = [0 min(N_Simulation*Ts,XMax)];
  
    
    tt = XLim(1):Ts:XLim(2);
    tt_sim = tt(1:k);
    tt_pred = tt(k:k+N-1);
    
    Var_x = cellfun(@(S) {diag(S)}, Sx);
    Std_x = sqrt([Var_x{:}]);
    
    for i = 1:nx
        ax = nexttile(i); 
        hold on
        if i == 1
            plot(tt,y_ref_all(i,1:numel(tt)),'r--','LineWidth',1.5);
        end
        plot(tt_sim,Mx_traj(i,1:k),'Color',[0 0.4470 0.7410]);
    
        % Plot variance of predicted states
        plot(tt_pred,Mx(i,1:N),'Color',Color_MPC)
        Mf_Plot_MeanVar(tt_pred,Mx(i,1:N),Std_x(i,1:N),Color_SIM);

        ylabel(sprintf('$x_{%d}$',i),'Interpreter','latex','FontSize',12)
        grid on
        box on
        axis tight
    
        YLim = ax.YLim;
        plot([1 1]*tt_sim(end),YLim,'k--')
        ylim(YLim);
        xlim(XLim);
    
        Logger.latexify_axis(ax,10);
    end
    
    for i = 1:nu
        axnr = 3*i;
        ax = nexttile(axnr); 
        hold on
        stairs(tt_sim,[Mu_traj(i,1:k-1),Mu(i,1)]);                
        stairs(tt_pred,Mu(i,1:N),'Color',Color_MPC)
    
        ylabel(sprintf('$u_{%d}$',i),'Interpreter','latex','FontSize',12)
        grid on
        box on
        axis tight
    
        YLim = ax.YLim;
        plot([1 1]*tt_sim(end),YLim,'k--')
        ylim(YLim);
        xlim(XLim);
    
        Logger.latexify_axis(ax,10);
    end
       
    nexttile(2);
    title(Title,Interpreter="latex",FontSize=12)
end

function fig = plotter_SIM_Ref(fname,fignr,XMax)
arguments
    fname,fignr
    XMax = Inf
end
    global G_COLOR_1 G_COLOR_2 G_COLOR_3 G_COLOR_4 G_COLOR_5 G_COLOR_6 G_COLOR_7 %#ok<NUSED> 

    Color_MPC = G_COLOR_1;
    Color_SIM = G_COLOR_2;

    load(fname, ...
        'N_Simulation','N_exec','Ts','nx','nu','ny','y_ref_all', ...
        'Mx_traj','Mu_traj','Mu','Mx','Sx','k','N');

    Title = sprintf([
        'State trajectories computed by MV-NMPC compared to the actual system trajectories. ' ...
        'Step $k = %d/%d$.'
        ],k,N_exec);

    fig = fig_new(140+fignr);
    tl = tiledlayout(1,1);
    tl.Padding = 'compact';
    tl.TileSpacing = 'tight';
    
    XLim = [0 max(N_Simulation*Ts,XMax)];
  
    
    tt = XLim(1):Ts:XLim(2);
    tt_sim = tt(1:k);
    tt_pred = tt(k:k+N-1);
    
    Var_x = cellfun(@(S) {diag(S)}, Sx);
    Std_x = sqrt([Var_x{:}]);
    
    ax = nexttile; 
    hold on
    plot(tt,y_ref_all(1,1:numel(tt)),'r--','LineWidth',1.5);
    plot(tt_sim,Mx_traj(1,1:k),'Color',[0 0.4470 0.7410]);

    % Plot variance of predicted states
    plot(tt_pred,Mx(1,1:N),'Color',Color_MPC)
    Mf_Plot_MeanVar(tt_pred,Mx(1,1:N),Std_x(1,1:N),Color_SIM);

    ylabel(sprintf('$x_{%d}$',1),'Interpreter','latex','FontSize',14)
    grid on
    box on
    axis tight

    YLim = ax.YLim;
    plot([1 1]*tt_sim(end),YLim,'k--')
    ylim(YLim);
    xlim(XLim);

    Logger.latexify_axis(ax,12);
end

function [Pl_Mu,Pl_2Sigma] = plot_mean_var_fast(tt,xx,xx_std,Color)
arguments
    tt,xx,xx_std,
    Color = [0 0.4470 0.7410]
end
    alpha = 2;
    Pl_2Sigma = plot(tt,xx' + xx_std'*(-alpha:2:alpha),'Color',Color);
end
