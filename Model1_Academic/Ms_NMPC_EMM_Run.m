%%
%  File: msc_LPV_MPC_vs_NMPC.m
%  Directory: 5_Sztaki20_Main/Models/01_QArm/v11_2021_11_29_closed_loop/6_lepes_egyetlen_gpmpc_nemlinearis
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. December 01. (2021b)
%

Ms_NMPC_EMM_Generate

%%

runID = datestr(datetime,'yymmmdd');
scriptID = 'Acad_Mod1_EMM_NMPC';
scriptDesc = 'Academic model 1. NMPC with EMM.';
fignr = 31;

helper = NLP_Solver_EMM.helper;

%% [NMPC]
% 
% 2022.01.06. (január  6, csütörtök), 13:49
% Time elapsed during NLP solver generation = 67.5562.
% NLP solver time = 223.672 (summarized for all k = 1,...,200).
% 
% Time elapsed during NLP solver generation = 67.5562.
% NLP solver time = 732.29 (summarized for all k = 1,...,651).

[f_fh,g_fh] = GP_Model_1('CT-nom');

G_reset

y_ref_all = Reference_output_1(Ts,[],2);

Mx_init = zeros(nx,1);
Sx_init = eye(nx) * 1e-2;

N_exec = size(y_ref_all,2) - N;
% N_exec = 200;
N_Simulation = N + N_exec - 2;

Q = 10000*eye(ny);
R = 10\eye(nu);

sol = {};
sol.x = zeros(size(helper.x));

odeoptions = odeset('MaxStep',0.01,'RelTol',1e-3);

Fnames = {};

Time_NLP = 0;

Mu_traj = zeros(nu,N_Simulation);
Mx_traj = [ Mx_init zeros(nx,N_Simulation) ];
for k = 1:N_exec
fprintf('k = %d:\n',k);

    Mxk = Mx_traj(:,k);
    yk_ref = y_ref_all(:,k+1:k+N);
    p = fpar_EMM_NMPC(Q,R,Mxk,Sx_init,yk_ref);

    % Solve MPC
    tic
    sol = NLP_Solver_EMM.solve(p,sol.x);
    Time_NLP = Time_NLP + toc;

    Mu = helper.get_value(Mu);
    Mx = [ Mxk helper.get_value(Mx) ];
    Sx = [ Sx_init helper.get_value(Sx) ];
    f = full(sol.f);

    Fnames = [ Fnames {sprintf('results/%s_%s_k%d.mat',scriptID,runID,k)} ];
    save(Fnames{end}, ... 'filepref','N_Simulation','N_exec','fignr','scriptDesc', ...
        'k','N','Ts','N_Simulation','N_exec', ...
        'nx','nu','ny','y_ref_all','Mu_traj','Mx_traj', ...
        'Mu','Mx','Sx','f','runID','scriptID')

    Mu_traj(:,k) = Mu(:,1);

    % Continuous-time simulation t in ( (k-1)*Ts , k*Ts ]
    f_ode = @(t,x) f_fh(x,Mu(:,1)) + g_fh(x(2,:));
    [~,xx] = ode45(f_ode,[0 Ts],Mx_traj(:,k),odeoptions);
    Mx_traj(:,k+1) = xx(end,:)';

    sol.x = fvar_EMM_NMPC(Mx(:,2:end),Sx{1:end-1},[Mu(:,1:end-1) zeros(nu,1)]);    
end

fprintf('\n');
fprintf('Time elapsed during NLP solver generation = %g.\n',Timer_Generate_NLP_Solver);
fprintf('NLP solver time = %g (summarized for all k = 1,...,%d).\n',Time_NLP,N_exec);

fname = sprintf('results/%s_%s_Summary.mat',scriptID,runID);
save(fname,'fignr','scriptDesc','Fnames','N_Simulation','N_exec', ...
    'N','Ts','nx','nu','ny','y_ref_all','Mu_traj','Mx_traj','runID','scriptID')

