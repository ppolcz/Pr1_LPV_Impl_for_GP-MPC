%%
%  File: msc_LPV_MPC_vs_NMPC.m
%  Directory: 5_Sztaki20_Main/Models/01_QArm/v11_2021_11_29_closed_loop/6_lepes_egyetlen_gpmpc_nemlinearis
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. December 01. (2021b)
%

Ms_LPV_MPC_EMM_Generate
GP_Exact_MM_Fast(hyp)

%%

% Maximum difference between solution and simulation:
Dx_Li_good = 1e-3;
tag = sprintf('_tol%02d',-log10(Dx_Li_good));

% Maximum number of iterations in a single time step:
NrIt_max = 50;

runID = [ datestr(datetime,'yymmmdd') tag];
scriptID = 'Acad_Mod1_EMM_LPV_MPC';
scriptDesc = 'Academic model 1. LPV-MPC with EMM.';
fignr = 30;

Sub_Sim = 1;
Sub_ReSub = 2;
Sub_Hybrid = 3;
Sub_Idx = Sub_Hybrid;
Sub = {
    'SimSub'
    'ReSub'
    'Hybrid'
    };

helper = QP_Solver_EMM.helper;

%% [LPV-MPC]
%
% 2022.01.06. (január  6, csütörtök), 13:24
% Time elapsed during NLP solver generation = 10.218.
% 
% Execution time (summarized for all k = 1,...,651):
%   QP solver time            = 26.4095.
%   Moment calculations       = 44.1686.
%   Simulation time (Matlab)  = 4.33871.
%   

[f_CT_fh,g_CT_fh] = GP_Model_1('CT-nom');

G_reset

y_ref_all = Reference_output_1(Ts,[],2);

Mx_init = zeros(nx,1);
Sx_init = eye(nx) * 1e-2;

N_exec = size(y_ref_all,2) - N;
% N_exec = 200;
N_Simulation = N + N_exec - 2;

Q = 10000*eye(ny);
R = 10\eye(nu);

sol = zeros(size(helper.x));

odeoptions = odeset('MaxStep',0.01,'RelTol',1e-3);

Iterations = [];
Fnames = {};
f_rep_traj = [];
f_SIM_traj = [];
f_dif_traj = [];
Dx_Li_traj = [];
u_apl_traj = [];
Sub_f_traj = [];

% Initial guess on the optimal inputs and predicted states
Tr.Ref.Mu = zeros(nu,N);
Tr.Ref.Mx = Mx_init * ones(1,N);
Tr.Ref.Sx = repmat({Sx_init},[1,N]);

% Initialize simulated state trajectory
Tr.Sim.Mx = Mx_init * ones(1,N+1);
Tr.Sim.Sx = repmat({Sx_init},[1,N+1]);

Time_Simulation_MatFun = 0;
Time_Simulation_CasADi = 0;
Time_Moments = 0;
Time_QP = 0;

Mu_traj = zeros(nu,N_Simulation);
Mx_traj = [ Mx_init zeros(nx,N_Simulation) ];
for k = 1:N_exec
fprintf('k = %d:\n',k);

    Mxk = Mx_traj(:,k);
    yk_ref = y_ref_all(:,k+1:k+N);

    Dx_Li_Ref_Opt = Inf;
    Dx_Li_Opt_Sim = Inf;
    Dx_Li_Ref_Sim = Inf;
    It = 0;
    Sub_Hybrid_Sim = false;
    while (Dx_Li_Ref_Sim > Dx_Li_good && It < NrIt_max) || ~Sub_Hybrid_Sim
        fprintf(' i = %d',It);

        % Preliminarily compute moments.
        tic
        Tr.Ref.Exp_z = Compute_mean(hyp,Tr.Ref.Mu,Tr.Ref.Mx,Tr.Ref.Sx,GP_args);
        Time_Moments = Time_Moments + toc;
        
        % arguments
        %     Q (1,1) = zeros(1,1)
        %     R (1,1) = zeros(1,1)
        %     Mx_init (2,1) = zeros(2,1)
        %     Sx_init (2,2) = zeros(2,2)
        %     Mx_ (2,50) = zeros(2,50)
        %     Mu_ (1,50) = zeros(1,50)
        %     Exp_z_ (1,50) = zeros(1,50)
        %     y_ref (1,50) = zeros(1,50)
        % end
        p = Fn_LPV_MPC_EMM_par(Q,R,Mxk,Sx_init,Tr.Ref.Mx,Tr.Ref.Mu,Tr.Ref.Exp_z,yk_ref);

        % Solve MPC
        tic
        % args = mskoptimset('Diagnostics','on','Display','iter');
        % args = mskoptimset('Diagnostics','off','Display','off');
        args = {};
        [sol,exitflag] = QP_Solver_EMM.solve_in_sim(p,sol,args);
        Time_QP = Time_QP + toc;
        if exitflag < 0
            error('Infeasible problem (gondolom numerikus hibak miatt)')
        end
        Tr.Opt.Mu = helper.get_value('Mu');
        Tr.Opt.Mx = [ Mxk helper.get_value('Mx') ];
        
        % Simulate nonlinear system with the computed control input
        Tr.Sim.Mx(:,1) = Mxk;
        Tr.Sim.Exp_z = zeros(nz,N);
        Tr.Sim.Var_z = cell(1,N);
        Tr.Sim.Cov_wz = cell(1,N);
        for i = 1:N
            tic
            [Tr.Sim.Exp_z(:,i),Tr.Sim.Var_z(i),Tr.Sim.Cov_wz(i)] ...
                = Compute_moments(hyp, ...
                    Tr.Opt.Mu(:,i), ...
                    Tr.Sim.Mx(:,i), ...
                    Tr.Sim.Sx(i), ...
                    GP_args);
            Time_Moments = Time_Moments + toc;
            % ---
            tic
            Tr.Sim.Mx(:,i+1) = GP_Model_1_Mx_kp1( ...
                    Tr.Sim.Mx(:,i), ...
                    Tr.Opt.Mu(:,i), ...
                    Tr.Sim.Exp_z(:,i));
            Tr.Sim.Sx{i+1} = GP_Model_1_Sx_kp1( ...
                    Tr.Sim.Mx(:,i), ...
                    Tr.Opt.Mu(:,i), ...
                    Tr.Sim.Sx{i}, ...
                    Tr.Sim.Var_z{i}, ...
                    Tr.Sim.Cov_wz{i}); 
            Time_Simulation_MatFun = Time_Simulation_MatFun + toc;
        end

        Dx_Li_Ref_Opt = max( sum( (Tr.Opt.Mx(:,1:N) - Tr.Ref.Mx(:,1:N)).^2 , 1) );
        Dx_Li_Opt_Sim = max( sum( (Tr.Sim.Mx(:,1:N) - Tr.Opt.Mx(:,1:N)).^2 , 1) );
        Dx_Li_Ref_Sim = max( sum( (Tr.Sim.Mx(:,1:N) - Tr.Ref.Mx(:,1:N)).^2 , 1) );

        Sub_final = Sub_Idx;
        Dx_Li_new = Dx_Li_Ref_Sim;
        Tr.Ref.Mu = Tr.Opt.Mu;
        Tr.Ref.Mx = Tr.Opt.Mx;
        if Sub_Idx == Sub_Sim || ( Sub_Hybrid_Sim && Sub_Idx == Sub_Hybrid )
            Tr.Ref.Mx = Tr.Sim.Mx;
            Tr.Ref.Sx = Tr.Sim.Sx;
            Sub_final = Sub_Sim;
        elseif Sub_Idx == Sub_Hybrid
            Tr.Ref.Sx = Tr.Sim.Sx;
            Sub_final = Sub_ReSub;

            if Dx_Li_Opt_Sim <= Dx_Li_good
                Dx_Li_new = Inf;    
                Sub_Hybrid_Sim = true;
                Sub_final = Sub_Sim;
            end
        end
        Tr.Ref.Mx = Tr.Ref.Mx(:,1:N);
    
        fprintf(' (%s) ref-opt = %g, opt-sim = %g, ref-sim = %g, \n', ...
            Sub{Sub_final},Dx_Li_Ref_Opt,Dx_Li_Opt_Sim,Dx_Li_Ref_Sim);  

        % f_rep_traj = [ f_rep_traj log10(f_rep + 1) ];
        % f_SIM_traj = [ f_SIM_traj log10(f_SIM + 1) ];
        % f_dif_traj = [ f_dif_traj log10(abs(f_rep - f_SIM) + 1) ];
        Dx_Li_traj = [ Dx_Li_traj Dx_Li_Ref_Sim ];
        Sub_f_traj = [ Sub_f_traj Sub_final ];
        u_apl_traj = [ u_apl_traj 0 ];

        Mat_Type = 'Iteration';
        Fnames = [ Fnames {sprintf('results/%s_%s_%s_k%d_it%d.mat',scriptID,runID,Sub{Sub_Idx},k,It)} ];
        save(Fnames{end},'N_exec','N_Simulation','scriptDesc', ...
            'Dx_Li_traj','u_apl_traj','Sub_final','Iterations', ...
            'Dx_Li_Ref_Sim','k','It','N','Ts', ...
            'nx','nu','ny','y_ref_all','Mu_traj','Mx_traj','Tr', ...
            'runID','scriptID','Mat_Type')

        Dx_Li_Ref_Sim = Dx_Li_new;

        It = It + 1;
    end
    Iterations = [Iterations It];


    u_apl_traj(end) = 1;
    Mu_traj(:,k) = Tr.Opt.Mu(:,1);

    % Continuous-time simulation t in ( (k-1)*Ts , k*Ts ]
    f_ode = @(t,x) f_CT_fh(x,Mu_traj(:,k)) + g_CT_fh(x(2,:));
    [~,xx] = ode45(f_ode,[0 Ts],Mx_traj(:,k),odeoptions);
    Mx_traj(:,k+1) = xx(end,:)';

    % Mat_Type = 'Step';
    % Fnames = [ Fnames {sprintf('%s/%s_%s_k%d.mat',DIR_RESULTS,scriptID,runID,k)} ];
    % save(Fnames{end}, 'k','N','Ts','N_Simulation','N_exec', ...
    %     'nx','nu','ny','y_ref_all','Mu_traj','Mx_traj', ...
    %     'runID','scriptID','scriptDesc','Mat_Type','Tr')
    
end

fprintf('\n\nTime elapsed during NLP solver generation = %g.\n',Timer_Generate_QP_Solver);
fprintf('\nExecution time (summarized for all k = 1,...,%d):\n',N_exec)
fprintf('  QP solver time            = %g.\n',Time_QP);
fprintf('  Moment calculations       = %g.\n',Time_Moments);
fprintf('  Simulation time (Matlab)  = %g.\n',Time_Simulation_MatFun);

fname = sprintf('results/%s_%s_Summary.mat',scriptID,runID);
save(fname,'scriptDesc','Fnames','N_Simulation','N_exec','Iterations','u_apl_traj', ...
    'N','Ts','nx','nu','ny','y_ref_all','Mu_traj','Mx_traj','runID','scriptID')

%% 

function [Exp_z,Var_z,Cov_wz] = Compute_moments(hyp,Mu,Mx,Sx,GP_args)
arguments
    hyp struct
    Mu
    Mx
    Sx
    GP_args
end

    nx = size(Mx,1);
    nw = numel(GP_args);
    K = zeros(size(Mu,1),nx);

    [~,N] = size(Mu);

    Mxu = [
        Mx(:,1:N)
        Mu(:,1:N)
        ];
    Mw = Mxu(GP_args,:);

    Exp_z = cell(1,N);
    Var_z = cell(1,N);
    Cov_wz = cell(1,N);

    for i = 1:N
        Sxu = [eye(nx) ; -K] * Sx{i} * [eye(nx) ; -K]';
        Sw = Sxu(GP_args,GP_args);
        [U,S,~] = svd(Sw);
        Sw = U * (S + eye(nw)*1e-10) * U';
        [Exp_z{i},Var_z{i},Cov_wz{i}] = GP_Exact_MM_Fast(hyp,Mw(:,i),Sw);
    end

    Exp_z = [ Exp_z{:} ];
end

function Exp_z = Compute_mean(hyp,Mu,Mx,Sx,GP_args)
arguments
    hyp struct
    Mu
    Mx
    Sx
    GP_args
end

    nx = size(Mx,1);
    nw = numel(GP_args);
    nz = numel(hyp);
    K = zeros(size(Mu,1),nx);

    [~,N] = size(Mu);

    Mxu = [
        Mx(:,1:N)
        Mu(:,1:N)
        ];
    Mw = Mxu(GP_args,:);

    Exp_z = zeros(nz,N);

    for i = 1:N
        Sxu = [eye(nx) ; -K] * Sx{i} * [eye(nx) ; -K]';
        Sw = Sxu(GP_args,GP_args);
        [U,S,~] = svd(Sw);
        Sw = U * (S + eye(nw)*1e-10) * U';
        Exp_z(:,i) = GP_Exact_MM_Fast(hyp,Mw(:,i),Sw,"MeanOnly",true);
    end

end
