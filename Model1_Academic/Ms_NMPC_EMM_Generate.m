%%
%  File: msc_Generate_NMPC.m
%  Directory: 5_Sztaki20_Main/Models/01_QArm/v11_2021_11_29_closed_loop/6_lepes_egyetlen_gpmpc_nemlinearis
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. December 01. (2021b)
%

% Load Phi, Gamma, Gamma_d, C, Ts, Actual_params, Hypothetical_params.
% Load QArm constants.
startup

G_reset

N = 50;

if ~exist('NLP_Solver_EMM','var') || NLP_Solver_EMM.userdata.N ~= N
Timer_90i3 = pcz_dispFunctionName('Generate solver object');

%% GP model to uncertain LTV
Timer_hefj = pcz_dispFunctionName('Construct GP model');

    import casadi.*

    [f_fh,Ed,C,hyp,Ts] = GP_Model_1;

    GP_args = hyp.GP_args;

    % Model dimensions
    [nx,nz] = size(Ed);
    [ny,~] = size(C);
    nu = ny;
    nw = numel(GP_args);

    %%%
    % Symbolic variables for the state moments
    Mx = SX.sym('Mx',nx,1);
    [Sx,Sx_half,Fun_Sx_vec,Fun_Sx_str] = Pcz_CasADi_Helper.create_sym('Sigma_x',nx,...
        str = "sym",...
        r1 = "Var_full",...
        r2 = "Var_half",...
        r3 = "f_vec", ...
        r4 = "f_str");

    %%%
    % Symbolic variables for the input moments: u = -K(x - mu_x) + mu_u
    K = SX(nu,nx);
    Mu = SX.sym('Mu',nu,1);

    %%%
    % Nominal model in the expectation
    f = f_fh(Mx,Mu);
    Jf = jacobian(f,[Mx;Mu]);

    %%%
    % Join Gaussian input distribution of the GP
    Mxu = [
        Mx
        Mu
        ];
    Mw = Mxu(GP_args);

    Sxu = [
         Sx    , -Sx*K'
        -K*Sx  ,  K*Sx*K'
        ];
    Sw = Sxu(GP_args,GP_args);
        
    %%%
    % Evaluate the fully symbolic GP expression
    [Exp_z,Var_z,Cov_wz,~,~] = GP_Exact_MM_Cas(hyp,Mw,Sw);

    Cov_xuz = SX(nx+nu,nz);
    Cov_xuz(GP_args,:) = Cov_wz;

    Fn_Mx_kp1 = Function('Fn_Mx_kp1',...
        {Mx,Mu,Sx_half},...
        {f + Ed*Exp_z},...
        {'mu_x','mu_u','Sigma_x'},...
        {'Mean dynamics with EMM'});

    ABE = [ Jf Ed ];
    Sx_kp1 = ABE*[Sxu Cov_xuz ; Cov_xuz' Var_z]*ABE';
    Fn_Sx_kp1 = Function('Fn_Sx_kp1',...
        {Mx,Mu,Sx_half},...
        {(Sx_kp1 + Sx_kp1')/2},...
        {'mu_x','mu_u','Sigma_x'},...
        {'Variance dynamics with EMM'});

pcz_dispFunctionEnd(Timer_hefj); % Construct GP model

%% Generate optimization model
Timer_v3ju = pcz_dispFunctionName('Construct prediction model');

    %%%
    % Initialized CasADi helper
    helper = Pcz_CasADi_Helper('SX');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Global constants 

    Q = helper.new_par('Q',ny,str="sym");
    R = helper.new_par('R',nu,str="sym");

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initial values

    % kezdeti allapot
    Mx_init = helper.new_par('Mx_init',nx);
    Sx_init = helper.new_par('Sx_init',nx,str="sym");

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Reference state trajectory

    y_ref = helper.new_par('y_ref',[ny,N]);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Free decision variables

    % Allapot varhato erteke
    Mx = [ Mx_init helper.new_var('Mx',[nx,N]) ];

    % Allapot szorasa
    Sx = [ {Sx_init} helper.new_var('Sx',nx,N,str="sym") ];

    % Robosztus szabalyozo bemenet
    Mu = helper.new_var('Mu',[nu,N],'lb',-1000,'ub',1000);
    
    status = PStatus(N);
    for k = 1:N

        Mx_kp1 = Fn_Mx_kp1(Mx(:,k),Mu(:,k),Sx{k});
        Sx_kp1 = Fn_Sx_kp1(Mx(:,k),Mu(:,k),Sx{k});

        helper.add_eq_con(Fun_Sx_vec( Sx_kp1 - Sx{k+1}   ));
        helper.add_eq_con(          ( Mx_kp1 - Mx(:,k+1) ));

        % Reference error in x
        refY_diff = y_ref(:,k) - C*Mx(:,k+1);
        helper.add_obj('Y_Mean',refY_diff.' * Q * refY_diff);

        % Trace of the covariance of sigma points
        % helper.add_obj('Y_Cov',trace(Q * C * Sx{k+1} * C'));

        % Reference error in x
        refU_diff = Mu(:,k);
        helper.add_obj('U_Mean',refU_diff.' * R * refU_diff);

        % Trace of the covariance of sigma points
        % helper.add_obj('U_Cov',trace(R * K * Sx{k+1} * K'));

        status.progress(k);
    end

    Timer_36e6 = pcz_dispFunctionName('Generate NLP solver');
    NLP_Solver_EMM = helper.get_nl_solver;
    NLP_Solver_EMM.userdata.N = N;
    pcz_dispFunctionEnd(Timer_36e6);

Timer_Generate_NLP_Solver = pcz_dispFunctionEnd(Timer_v3ju); % Construct prediction model

%%

% helper.gen_par_mfun('f_par','sim_qarm_gpmpc_actual/Solver parameter organizer',true)
helper_func_dir = [pwd filesep 'helper'];
if ~exist(helper_func_dir,"dir")
    mkdir(helper_func_dir)
end
f_par_NLP_EMM = helper.gen_par_mfun('fpar_EMM_NMPC',helper_func_dir);
f_var_NLP_EMM = helper.gen_var_mfun('fvar_EMM_NMPC',helper_func_dir);
addpath(helper_func_dir)

pcz_dispFunctionEnd(Timer_90i3); % Generate solver object
end % if ~exist('NLP_Solver_EMM','var')
