%%
%  File: msc_Generate_NMPC.m
%  Directory: 5_Sztaki20_Main/Models/01_QArm/v11_2021_11_29_closed_loop/6_lepes_egyetlen_gpmpc_nemlinearis
%  Author: Peter Polcz (ppolcz@gmail.com) 
%  
%  Created on 2021. December 01. (2021b)
%

% Load Phi, Gamma, Gamma_d, C, Ts, Actual_params, Hypothetical_params.
% Load QArm constants.
startup

G_reset

N = 50;

if ~exist('QP_Solver_EMM','var') || QP_Solver_EMM.userdata.N ~= N
Timer_asd4 = pcz_dispFunctionName('Generate solver object');

%% GP model to uncertain LTV
Timer_hefj = pcz_dispFunctionName('Construct GP model');

    %%%
    % In this section: variables with '_' stand for free decision variables
    %                  variables without '_' are precomputed.

    import casadi.*

    [f_fh,Ed,C,hyp,Ts] = GP_Model_1;

    GP_args = hyp.GP_args;

    % Model dimensions
    [nx,nz] = size(Ed);
    [ny,~] = size(C);
    nu = ny;
    nw = numel(GP_args);

    %%%
    % [PRECOMPUTED] Symbolic variables for the precomputed state moments
    Mx = SX.sym('Mx',nx,1);
    [Sx,Sx_half,Fun_Sx_vec,Fun_Sx_str] = Pcz_CasADi_Helper.create_sym('Sigma_x',nx,...
        str = "sym",...
        r1 = "Var_full",...
        r2 = "Var_half",...
        r3 = "f_vec", ...
        r4 = "f_str");

    %%%
    % [VARIABLE] Symbolic variables for the free state moments
    Mx_ = SX.sym('Mx_',nx,1);
    [Sx_,Sx_half_] = Pcz_CasADi_Helper.create_sym('Sigma_x',nx,...
        str = "sym",...
        r1 = "Var_full",...
        r2 = "Var_half");

    %%%
    % [PRECOMPUTED] Symbolic variables for the input moments: u = -K(x - mu_x) + mu_u
    K = SX(nu,nx); % (symbolic zeros)
    Mu = SX.sym('Mu',nu,1);
    
    %%%
    % [VARIABLE] Symbolic variables for the input moments: u = -K(x - mu_x) + mu_u
    Mu_ = SX.sym('Mu_',nu,1);
    
    %%%
    % Nominal model evaluated in the PRECOMPUTED values
    f = f_fh(Mx,Mu);

    %%%
    % Jacobian of the nominal model in the terms of the PRECOMPUTED values
    Jf = jacobian(f,[Mx;Mu]);

    %%%
    % [PRECOMPUTED] Symbolic variables for the precomputed EXACT moments
    Exp_z = SX.sym('Exp_z',nz,1);
    Cov_wz = SX.sym('Cov_wx',nw,nz);
    [Var_z,Var_z_half,Fun_Var_z_vec] = Pcz_CasADi_Helper.create_sym('Var_z',nz,...
        str = "sym",...
        r1 = "Var_full",...
        r2 = "Var_half",...
        r3 = "f_vec");
    
    % Not all state and input variables take part in the GP model, thus:
    Cov_xuz = SX(nx+nu,nz);
    Cov_xuz(GP_args,:) = Cov_wz;

    Fn_Mx_kp1 = Function('Fn_Mx_kp1',...
        {Mx_,Mu_,Mx,Mu,Exp_z},...
        {Jf*[Mx_;Mu_] + f - Jf*[Mx;Mu] + Ed*Exp_z},...
        {'Mx_','Mu_','Mx','Mu','Exp_z'},...
        {'Mean dynamics with EMM'});

    Sxu = [
         Sx_    , -Sx_*K'
        -K*Sx_  ,  K*Sx_*K'
        ];
    ABE = [ Jf Ed ];
    Sx_kp1 = ABE*[Sxu Cov_xuz ; Cov_xuz.' Var_z]*ABE.';
    Fn_Sx_kp1 = Function('Fn_Sx_kp1',...
        {Sx_half_,Mx,Mu,Var_z_half,Cov_wz},...
        {(Sx_kp1 + Sx_kp1')/2},...
        {'Sx_','Mx','Mu','Var_z','Cov_wz'},...
        {'Variance dynamics with EMM'});

pcz_dispFunctionEnd(Timer_hefj); % Construct GP model

%%

u = sym('u',[nu,1]);
x = sym('x',[nx,1]);
Sx_vec = sym('Sx',[nx*(nx+1)/2 1]);

K = zeros(nu,nx);

f_sym = f_fh(x,u);
ABE_sym = [ jacobian(f_sym,[x;u]) Ed ];

[i,j] = meshgrid(1:nx);
Idx = zeros(nx,nx);
Idx(i(:) <= j(:)) = 1:nx*(nx+1)/2;
Idx = (Idx + Idx') - diag(diag(Idx));

Sx_sym = Sx_vec(Idx);
Sxu_sym = [
     Sx_sym    , -Sx_sym*K'
    -K*Sx_sym  ,  K*Sx_sym*K'
    ];

Exp_z_sym = sym('Exp_z',[nz,1]);

Cov_xuz_sym = sym(zeros(nx+nu,nz));
Cov_wz_sym = sym('Cov_wx',[nw,nz]);
Cov_xuz_sym(GP_args,:) = Cov_wz_sym;

Var_z_sym = sym('Var_z');

Sx_kp1_sym = ABE_sym*[Sxu_sym Cov_xuz_sym ; Cov_xuz_sym.' Var_z_sym]*ABE_sym.';
Sx_kp1_sym = (Sx_kp1_sym + Sx_kp1_sym.')/2;

matlabFunction(f_sym + Ed*Exp_z_sym,'Vars',{x,u,Exp_z_sym}, ...
    'File','GP_Model_1_Mx_kp1','Optimize',true);

matlabFunction(Sx_kp1_sym,'Vars',{x,u,Sx_sym,Var_z_sym,Cov_wz_sym}, ...
    'File','GP_Model_1_Sx_kp1','Optimize',true);

%% Generate optimization model
Timer_v3ju = pcz_dispFunctionName('Construct prediction model');

    %%%
    % In this section we switch the roles (notations): variables with '_'
    % stand for precomputed values

    %%%
    % Initialized CasADi helper
    helper = Pcz_CasADi_Helper('SX');

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Global constants 

    Q = helper.new_par('Q',ny,str="sym");
    R = helper.new_par('R',nu,str="sym");

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initial values

    % kezdeti allapot
    Mx_init = helper.new_par('Mx_init',nx);
    Sx_init = helper.new_par('Sx_init',nx,str="sym");

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Iterated PRECOMPUTED time series

    % Preliminarily computed states
    Mx_ = helper.new_par('Mx_',[nx,N]);

    % Preliminarily computed inputs
    Mu_ = helper.new_par('Mu_',[nu,N]);

    % Preliminarily computed exact moments
    Exp_z_ = helper.new_par('Exp_z_',[nz,N]);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Reference state trajectory

    y_ref = helper.new_par('y_ref',[ny,N]);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Free decision variables

    % Expected states
    Mx = [ Mx_init helper.new_var('Mx',[nx,N]) ];

    % Variance of states
    Sx = [ {Sx_init} helper.new_var('Sx',nx,N,str="sym") ];

    % Nominal input values
    Mu = helper.new_var('Mu',[nu,N],'lb',-1000,'ub',1000);
    
    status = PStatus(N);
    for k = 1:N

        Mx_kp1 = Fn_Mx_kp1(Mx(:,k),Mu(:,k),Mx_(:,k),Mu_(:,k),Exp_z_(:,k));

        helper.add_eq_con(Mx_kp1 - Mx(:,k+1));

        % Reference error in x
        refY_diff = y_ref(:,k) - C*Mx(:,k+1);
        helper.add_obj('Y_Mean',refY_diff.' * Q * refY_diff);

        % Reference error in x
        refU_diff = Mu(:,k);
        helper.add_obj('U_Mean',refU_diff.' * R * refU_diff);

        status.progress(k);
    end

    Timer_36e6 = pcz_dispFunctionName('Generate QP solver');
    QP_Solver_EMM = helper.get_qp_solver;
    QP_Solver_EMM.userdata.N = N;
    pcz_dispFunctionEnd(Timer_36e6);

pcz_dispFunctionEnd(Timer_v3ju); % Construct prediction model

%%

helper_func_dir = [pwd filesep 'helper'];
if ~exist(helper_func_dir,"dir")
    mkdir(helper_func_dir)
end
f_par_NLP_EMM = helper.gen_par_mfun('fpar_LPV_MPC_EMM',helper_func_dir);
f_var_NLP_EMM = helper.gen_var_mfun('fvar_LPV_MPC_EMM',helper_func_dir);
addpath(helper_func_dir)

Timer_Generate_QP_Solver = pcz_dispFunctionEnd(Timer_asd4); % Generate solver object
end % if ~exist('NLP_Solver_EMM','var')
